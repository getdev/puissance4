#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 21:21:09 2021

@author: gaetan
"""

from random import randint
from time import sleep

colonnes=[[], [], [], [], [], [], []]
nli=6
ncol=7

def fctAffiche():
    global colonnes
    # On parcours les lignes
    for i in range(nli):
        affiche='|'
        fond='--'
        for j in range(ncol):
            fond+='---'
            try:
                affiche += ' ' + colonnes[j][nli-1-i] + ' '
            except:
                affiche += ' . '
        affiche += '|'
        print(affiche)
    print(fond)
    print('  1  2  3  4  5  6  7')
    print(fond)

    #print(colonnes)


# joueur = X ou O, participant humain = True, machine = False
def coup(joueur, participant):
    '''
    Cette fonction permet au joueur d'executer son coup. Et verifie que la
    partie ne soit pas fini.
    Renvoi True ou False fonction de si la partie est fini ou non.
    '''
    global colonnes
    # Ici on a une saisi, l'indice de colonne est décalé de 1.
    colonne=-1
    while not ( colonne<=(ncol-1) and colonne>=0 and coupPossible(colonne, participant) ):
        colonne = choix(joueur, participant)
    colonnes[colonne].append(joueur)
    if verifieGagne(joueur,colonne):
        print ('=========\nJoueur ' + str(joueur) + ' a GAGNE\n=========')
        return True
    return False

def verifieGagne(joueur, coup):
    '''
    Le "coup" correspond au dernier coup effectue par "joueur".
    Il faut verifie que :
    - il n'y ait pas 4 pions alignés à l'horizontal
    - il n'y ait pas 4 pions alignés à la vertical
    - il n'y ait pas 4 pions alignés en diagonal
    '''
    print(' ==> Joueur et coup : ' + str(joueur) + ' ' + str(coup + 1))
    return verifVertical(joueur, coup) or verifHorizontal(joueur, coup) or verifDiagonal(joueur, coup)

def verifVertical(joueur, coup):
    '''
    Verifie que les 3 precedents pions de la colonne "coup" sont
    identique à "joueur"
    '''
    global colonnes
    alignement=0
    place=len(colonnes[coup]) - 1
    if place >= 3:
        #print(colonnes[coup])
        for num in range(0, 4):
            #print ('place : ' + str(place) + '; num : ' + str(num))
            if joueur == colonnes[coup][place - num]:
                alignement += 1
                #print('alignement : ' + str(alignement))
            else:
                #print('break')
                break
        if alignement == 4:
            print('Vertical colonne ' + str(coup+1) )
            return True
    return False

def verifDiagonal(joueur, coup):
    '''
    Verifie que les diagonales sont bonnes.
    '''
    global colonnes
    hauteurBase=len(colonnes[coup]) - 1
    # Verifie avec  les decalages bas gauche > haut droit et haut gauche vers bas droit
    # - cas A : bg -> hd : pas negatif avant et positif après par rapport à 'place'
    # - cas B : hg -> bd : pas positif avant et negatif après par rapport à 'place'
    # Comme pour l'horizontale, on regarde les pions avant et apres
    for pas in [1, -1]: # Coef multiplicateur pour faire les 2 diagonales
        alignement = 1 # le pion qui vient d'etre joue
        for i in range(1, coup + 1): # Parcours a reculons la liste 'colonnes'
            place = hauteurBase - ( i * pas) # En fonction du cas, la hauteur va augmenter ou descendre.
            inverse = coup - i
            if place >= 0:
                #print('Coup=', coup, '; hteurBase=', hauteurBase, '; colonne=', inverse, '; hauteur=', place)
                try: # Si l'element n'existe pas dans la liste on n'arrete de regarder dans ce sens.
                    valeur = colonnes[inverse][place]
                except:
                    #print('break')
                    break
                if valeur == joueur:
                    alignement += 1
                else:
                    #print('diff avant')
                    break
        for i in range(coup + 1, len(colonnes)):
            # La place est en référence à 'coup' la colonne de base.
            place = hauteurBase + ( (coup - i) * pas) 
            if place >= 0:
                #print('Coup=', coup, '; hteurBase=', hauteurBase, '; colonne=', i, '; hauteur=', place)
                try:
                    valeur = colonnes[i][place]
                except:
                    #print('break')
                    break
                if valeur == joueur:    
                    alignement += 1
                else:
                    #print('diff apres')
                    break

        #print('alignement D : ' + str(alignement))
        if alignement >= 4:
            return True
    
    return False


def verifHorizontal(joueur, coup):
    '''
    Verifie qu'il y a une ligne de 4 pion
    '''
    global colonnes
    alignement = 1 # le pion qui vient d'etre joue
    place=len(colonnes[coup]) - 1
    # Decompose en pion avant (colonne < coup) et pion apres (colonne > coup)
    # range ne s'arrete a <coup
    # Avant
    inverse = 0 # Cette initialisation sert pour l'affichage de la ligne gagnante.
    #Si le dernier pion joue est dans la première colonne, inverse ne serait jamais
    #initialisé.
    for i in range(1, coup + 1):
        inverse = coup - i
        try:
            valeur = colonnes[inverse][place]
        except:
            break
        if valeur == joueur:
            alignement += 1
        else:
            break
    # Apres
    for i in range(coup + 1, len(colonnes)):
        try:
            valeur = colonnes[i][place]
        except:
            break
        if valeur == joueur:
            alignement += 1
        else:
            break
    #print('alignement H : ' + str(alignement))
    if alignement >= 4:
        print('Victoire de Joueur ' + joueur + ' horizontal ' + str(inverse+1) +'-'+ str(i) + ' à hauteur ' + str(place + 1))
        return True
    return False
# Cette fonction retourne le choix de colonne fait par l'ordi ou le joueur en fonction
def choix(joueur, participant):
    if participant:
        retour = int(input('Quel colonne (joueur ' + joueur + '): ')) - 1
    else:
        # On est avec l'ordi qui fait son choix :
        sleep(0.5)
        retour = randint(0, 6)
    return retour

# Verifie la possibilité du coup, et affiche un message si je joueur est humain
def coupPossible(colonne, participant):
    global colonnes
    # print (str(len(colonnes[colonne])) + ' : ' + str(colonnes[colonne]))
    if len(colonnes[colonne]) == nli:
        if participant:
            print('Colonne ' + str(colonne) + ' déjà remplie !')
        return False
    else:
        return True

def listeParticipants():
    '''
    On défini ici les joueurs qui vont participer Ordi (=False), Humain (=True).
    '''
    global participants
    rep=0
    while rep < 1 or rep > 4:
        print('Faite votre choix dans les joueurs :')
        print('1 - Humain VS Humain')
        print('2 - Ordi VS Humain')
        print('3 - Humain VS Ordi')
        print('4 - Ordi VS Ordi')
        rep = int(input('Choix : '))
    if rep == 2:
        participants = [ False, True]
    elif rep == 3:
        participants = [ True, False]
    elif rep == 4:
        participants = [ False, False]
            
            

# On fait une liste de 2 joueurs, la parité du coup donnera l'indicateur (X ou O) du joueur
joueurs = ['X', 'O']
# Pour la même raison on fait une liste de participants 'Personne' = True ou 'Ordi' = False
participants = [True, True]
listeParticipants()
fctAffiche()
for i in range(nli*ncol):
    fin = coup(joueurs[i % 2], participants[i % 2])
    fctAffiche()
    if fin:
        break
    
